import ForwardIcon from '@mui/icons-material/Forward';

function BackwardIcon() {
  return <ForwardIcon style={{ transform: 'rotate(180deg)' }} />;
}

export default BackwardIcon;
