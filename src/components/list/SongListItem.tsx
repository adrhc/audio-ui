import { ListItem, IconButton, ListItemButton, ListItemText, Stack } from '@mui/material';
import { useCallback, useContext } from 'react';
import { songEqual } from '../../domain/track';
import { SelectableSong, Song } from '../../domain/song';
import SongListItemAvatar from './SongListItemAvatar';
import CheckBoxOutlinedIcon from '@mui/icons-material/CheckBoxOutlined';
import { AppContext } from '../../hooks/AppContext';

type SongHandler = (song: Song) => void;

type SongListItemParam = {
  prevSongsCount: number;
  lastUsed?: Song | null;
  song: SelectableSong;
  index: number;
  onAdd?: SongHandler;
  onInsert?: SongHandler;
  onDelete?: SongHandler;
  onClick?: SongHandler;
  onSelect?: (song: SelectableSong) => void;
};

function SongListItem({
  prevSongsCount,
  lastUsed,
  song,
  index,
  onAdd,
  onInsert,
  onDelete,
  onClick,
  onSelect,
}: SongListItemParam) {
  // console.log(`[SongListItem]`, { song, lastUsed });
  const { currentSong } = useContext(AppContext);
  const isLastUsed = useCallback(
    (song: Song) => {
      return song.uri == lastUsed?.uri;
    },
    [lastUsed]
  );

  const songCount = 1 + prevSongsCount + index;
  const showActions = onInsert || onAdd || onDelete || (song.selected && onSelect);
  return (
    <ListItem
      disablePadding
      className={`${song.type ?? ''} ${isLastUsed(song) ? 'last-used-song' : ''}`}
      // https://react.dev/learn/rendering-lists#keeping-list-items-in-order-with-key
      // JSX elements directly inside a map() call always need keys!
      // adrhc: hence the key must be used on the component (i.e. SongListItem)!
      // https://react.dev/learn/rendering-lists#why-does-react-need-keys
      // Note that your components won’t receive key as a prop. It’s only used as a hint by React itself.
      //
      // Using songCount because a song could be twice in the list!
      // key={`${songCount}/${song.uri}`}
      secondaryAction={
        showActions && (
          <Stack className="action">
            {onInsert && (
              <IconButton className="add-btn" onClick={() => onInsert(song)}>
                <img src="btn/four-arrows-inside-line-icon.svg" />
              </IconButton>
            )}
            {onAdd && (
              <IconButton className="insert-btn" onClick={() => onAdd(song)}>
                <img src="btn/plus-square-line-icon.svg" />
              </IconButton>
            )}
            {onDelete && (
              <IconButton className="del-btn" onClick={() => onDelete(song)}>
                <img src="btn/recycle-bin-line-icon.svg" />
              </IconButton>
            )}
            {song.selected && onSelect && (
              // <CheckBoxOutlinedIcon className="secondary-action-btn" onClick={() => onSelect(song)} />
              <IconButton className="select-btn" onClick={() => onSelect(song)}>
                <CheckBoxOutlinedIcon />
              </IconButton>
            )}
          </Stack>
        )
      }
    >
      <ListItemButton selected={songEqual(song, currentSong)} onClick={() => (onClick ?? onSelect)?.(song)}>
        <SongListItemAvatar song={song} />
        <ListItemText primary={`${songCount}. ${song.title}`} secondary={song.formattedUri} />
      </ListItemButton>
    </ListItem>
  );
}

export default SongListItem;
