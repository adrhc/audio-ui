import LocalLibraryEditTemplatePage from './LocalLibraryEditTemplatePage';

function LocalLibraryEditFromPlayingPage() {
  return <LocalLibraryEditTemplatePage playlistEditorPath="/playlist-edit-from-playing" />;
}

export default LocalLibraryEditFromPlayingPage;
