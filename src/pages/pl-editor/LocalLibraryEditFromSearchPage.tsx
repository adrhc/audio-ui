import LocalLibraryEditTemplatePage from './LocalLibraryEditTemplatePage';

function LocalLibraryEditFromSearchPage() {
  return <LocalLibraryEditTemplatePage playlistEditorPath="/playlist-edit-from-search" />;
}

export default LocalLibraryEditFromSearchPage;
