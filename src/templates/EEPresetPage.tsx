import { ReactNode, useEffect } from 'react';
import PageTemplate from './PageTemplate';
import { LoadingState, SetFeedbackState, SetLoadingState } from '../lib/sustain/types';
import { EEPreset } from '../infrastructure/easyeffects/types';
import { getPreset } from '../infrastructure/easyeffects/easyeffects';
import { SustainVoidFn } from '../hooks/useSustainableState';
import { useParams } from 'react-router-dom';

interface EEPresetPageParam {
  state: LoadingState<EEPreset>;
  sustain: SustainVoidFn<EEPreset>;
  setState: SetLoadingState<EEPreset>;
  bottom?: ReactNode;
  children: ReactNode;
}

function EEPresetPage({ state, sustain, setState, bottom, children }: EEPresetPageParam) {
  const { preset } = useParams();

  useEffect(() => {
    // console.log(`[PresetEditPage.useEffect] preset:`, preset);
    preset && sustain(getPreset(preset), { error: `Failed to load ${preset}!` });
  }, [preset, sustain]);

  return (
    <PageTemplate
      state={state}
      setState={setState as SetFeedbackState}
      title={`Preset: ${preset}`}
      bottom={bottom}
    >
      {children}
    </PageTemplate>
  );
}

export default EEPresetPage;
