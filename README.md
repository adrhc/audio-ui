# Audio UI
UI integrating features from Mopidy HTTP, EasyEffects and KEF LSX speakers.

# Resources
https://github.com/wwmm/easyeffects
https://github.com/kraih/kefctl
https://docs.mopidy.com/latest/ext/http/
https://mopidy.com/
